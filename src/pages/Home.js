import React from "react";
import {
  FaReact,
  FaAngular,
  FaJava,
  FaBootstrap,
  FaJsSquare,
  FaNodeJs,
  FaDatabase,
  FaPython,
  FaNpm,
  FaHtml5,
  FaCss3,
  FaGithub,
} from "react-icons/fa";
import { SiTailwindcss, SiIonic, SiMongodb, SiNestjs } from "react-icons/si";
import Layout from "../components/Layout";
import AOS from "aos";
AOS.init({
  duration: 1000,
});

function Home() {
  return (
    <Layout>
      <div>
        {/* Intro Section */}

        <div className="h-screen bg-sky-700">
          <div
            className="grid md:grid-cols-1 grid-cols-2 h-screen items-center border-4 md:border-0
          mx-10 bg-sky-700
          border-white transform rotate-12 md:rotate-0 "
          >
            <div className="h-1/2">
              <lottie-player
                src="https://assets5.lottiefiles.com/packages/lf20_kyu7xb1v.json"
                background="transparent"
                speed="1"
                loop
                autoplay
              ></lottie-player>
            </div>

            <div className="font-bold text-white md:px-5">
              <h1 className="text-7xl md:text-4xl" data-aos="slide-right">
                Hi all , I'm
                <b className="text-yellow-500"> Mohamed lemine</b>{" "}
              </h1>
              <h1 className="text-4xl md:text-xl" data-aos="slide-left">
                Fullstack <b className="text-red-500">Developer</b> 
              </h1>
            </div>
          </div>
        </div>

        {/* Technologies Section */}

        <div className="mt-20">
          <h1
            className="text-4xl text-sky-900 font-bold text-center my-8"
            data-aos="slide-up"
          >
            Technologies I USE
          </h1>
          <div className="grid md:grid-cols-1 grid-cols-4">
            <FaReact
              size={180}
              color="#00d8ff"
              className="w-full text-center mt-20"
            />
            <FaAngular
              size={180}
              color="#c62334"
              className="w-full text-center mt-10 animate-bounce"
            />
            <FaNodeJs
              size={180}
              color="#80bd00"
              className="w-full text-center mt-20 animate-bounce"
            />
            <SiTailwindcss
              size={180}
              color="#06b6d4"
              className="w-full text-center mt-20"
            />
            <FaJsSquare
              size={180}
              color="#f7df1d"
              className="w-full text-center mt-20 animate-bounce"
            />
            <FaBootstrap
              size={180}
              color="#7211f6"
              className="w-full text-center mt-20"
            />
            <SiIonic
              size={180}
              color="#448aff"
              className="w-full text-center mt-20"
            />
            <FaJava
              size={180}
              color="#f0931c"
              className="w-full text-center mt-20 animate-bounce"
            />
            <SiMongodb
              size={180}
              color="#10ab50"
              className="w-full text-center mt-20"
            />
            <FaDatabase
              size={180}
              color="#fbca3f"
              className="w-full text-center mt-20 animate-bounce"
            />
            <FaPython
              size={180}
              color="#376f9e"
              className="w-full text-center mt-20 animate-bounce"
            />
            <FaNpm
              size={180}
              color="#cc3838"
              className="w-full text-center mt-20"
            />
            <FaHtml5
              size={180}
              color="#f06628"
              className="w-full text-center mt-20 animate-bounce"
            />
            <SiNestjs
              size={180}
              color="#e0234e"
              className="w-full text-center mt-20"
            />
            <FaGithub
              size={180}
              color="#1b1f23"
              className="w-full text-center mt-20"
            />
            <FaCss3
              size={180}
              color="#2a65f1"
              className="w-full text-center mt-20 animate-bounce"
            />
          </div>
        </div>

        {/* Javascript Buff */}

        <div className="my-20">
          <div className="text-center h-52 bg-primary">
            <h1 className="text-white font-bold text-2xl py-8">
              CRAZY FULL STACK DEVELOPER WHO WANTS TO EXPLORE EVERY TECH STACK
            </h1>
          </div>

          <div className="md:mx-5 mx-32 shadow-2xl bg-gray-50 -mt-20 rounded-lg hover:bg-gray-700 hover:text-white">
            <div className="h-96" data-aos="zoom-in">
              <lottie-player
                src="https://assets6.lottiefiles.com/packages/lf20_yd8fbnml.json"
                background="transparent"
                speed="1"
                loop
                autoplay
              ></lottie-player>
            </div>

            <p className="text-xl my-5 font-semibold md:px-5 px-14 py-10 text-center">
              Javascript is one of the most top-ranked programming languages
              because of its ubiquitous use on all platforms ans mass adoption.
              Main use cases: Web Development.
            </p>
          </div>
        </div>

        {/* Dev Stack Section */}

        <div className="my-20">
          <div className="text-center h-52 bg-red-500">
            <h1 className="text-white font-bold text-2xl py-8">My DEV Stack</h1>
          </div>

          <div className="md:mx-5 mx-32 shadow-2xl bg-gray-50 -mt-20 rounded-lg hover:bg-gray-700 hover:text-white">
            <div className="h-96" data-aos="zoom-in">
              <lottie-player
                src="https://assets2.lottiefiles.com/packages/lf20_u4jjb9bd.json"
                background="transparent"
                speed="1"
                loop
                autoplay
              ></lottie-player>
            </div>

            <div className="grid md:grid-cols-1 grid-cols-3 p-5">
              <div>
                <h1 className="text-xl font-bold">&lt; Front End /&gt;</h1>
                <hr />
                <p className="font-semibold mt-2">HTML/CSS</p>
                <p className="font-semibold mt-2">Angular</p>

                <p className="font-semibold mt-2">React</p>
                <p className="font-semibold mt-2">Javascript</p>
                <p className="font-semibold mt-2">Redux</p>
              </div>

              <div className="text-center">
                <h1 className="text-xl font-bold">UI / UX</h1>
                <hr />
                <p className="font-semibold mt-2">Botstrap</p>
                <p className="font-semibold mt-2">Tailwind</p>

                <p className="font-semibold mt-2">Ant Design</p>
                <p className="font-semibold mt-2">Material UI</p>
                <p className="font-semibold mt-2">Semantic UI</p>
              </div>

              <div className="text-right">
                <h1 className="text-xl font-bold">
                  &#x7B; Back End && DB &#x7D;
                </h1>
                <hr />
                <p className="font-semibold mt-2">Node JS</p>
                <p className="font-semibold mt-2">Express JS</p>

                <p className="font-semibold mt-2">Mongo DB</p>
                <p className="font-semibold mt-2">PHP</p>
                <p className="font-semibold mt-2">MySQL</p>
              </div>
            </div>
          </div>
        </div>

        {/* Dev Info */}

        <div>
          <h1 className="text-4xl text-gray-500 text-center font-bold">
            Who is Med Lemine ?
          </h1>

          <div className="h-screen relative text-gray-800">
            <div className="h-full">
              <lottie-player
                src="https://assets5.lottiefiles.com/packages/lf20_dcatp5cr.json"
                background="transparent"
                speed="1"
                loop
                autoplay
              ></lottie-player>
            </div>

            <div className="absolute inset-0 flex flex-col items-center justify-center">
              <h1 className="text-2xl font-bold">
                Hi....
                <hr />
                <pre className="text-xl md:text-sm my-5 font-mont font-semibold">
                  {JSON.stringify(
                    {
                      name: "Hamdinou Med Lemine",
                      age: null,
                      gender: "male",
                      country: "MAURITANIA",
                    },
                    null,
                    2
                  )}
                </pre>
              </h1>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default Home;
