import React from "react";
import Layout from "../components/Layout";
import coursesData from "../resourses/courses";
import AOS from "aos";
AOS.init({
  duration: 1000,
});

function Courses() {
  return (
    <Layout>
      <div>
        <div>
          <div className="h-screen relative">
            <div className="h-3/4 bg-sky-700 pt-20">
              <lottie-player
                src="https://assets7.lottiefiles.com/packages/lf20_z01bika0.json"
                background="transparent"
                speed="1"
                loop
                autoplay
              ></lottie-player>
            </div>
            <div className="absolute left-0 right-0 -bottom-34">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                <path
                  fill="#0369a1"
                  fill-opacity="1"
                  d="M0,256L48,224C96,192,192,128,288,96C384,64,480,64,576,90.7C672,117,768,171,864,170.7C960,171,1056,117,1152,101.3C1248,85,1344,107,1392,117.3L1440,128L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"
                ></path>
              </svg>
            </div>
          </div>
        </div>
      </div>

      <div className="mt-20 md:mt-5">
        <p className="text-xl font-semibold text-center">
          Good ideas are not adopted automatically. They must be driven into
          practice with courageous patience
        </p>
        <h1 className="text-4xl text-center font-bold mt-5">Because </h1>
      </div>

      <div className="font-bold text-center bg-gray-500 mt-20 m-20 p-20 text-white rounded-tl-full rounded-br-full md:mx-5">
        <h1 className="text-8xl md:text-2xl" data-aos='slide-down'>LEARNING IS...</h1>
        <h1 className="text-8xl md:text-2xl" data-aos='slide-up'>CONTINUOUS</h1>
      </div>

      <div className="grid mt-20 md:grid-cols-1 grid-cols-3 items-center justify-center gap-10 mx-20 md:mx-2">
        {coursesData.map((course) => {
          return (
            <div>
              <div className=" relative border-2 text-center rounded-tr-3xl rounded-bl-3xl border-gray-400">
                <img
                  src={course.image}
                  className="w-full h-60 text-center rounded-tr-3xl rounded-bl-3xl"
                />

                <div className="absolute inset-0 flex items-center justify-center flex-col opacity-0 bg-black hover:opacity-80 rounded-tr-3xl rounded-bl-3xl">
                  <h1 className="text-4xl font-semibold text-white">
                    {course.title}
                  </h1>
                  <button className="border-2 rounded border-white p-2 hover:bg-green-500 px-5 mt-5 text-white">
                    DEMO
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </Layout>
  );
}

export default Courses;
