import React from "react";
import {
  FaFacebook,
  FaInstagram,
  FaLinkedin,
  FaGitlab,
  FaGithub,
  FaTwitter,
} from "react-icons/fa";
import { GrMail } from "react-icons/gr";
import { IconContext } from "react-icons";

export default function Footer() {
  const socialMediaLinks = {
    github: "https://github.com/Med-lemineHmd",
    linkedin: "https://www.linkedin.com/in/saadpasta/",
    gmail: "mailto:medlamine.hmd@gmail.com",
    gitlab: "https://gitlab.com/Med-lemineHmd",
    facebook: "https://www.facebook.com/mohamedlemin.hamdinou/",
    twitter: "https://twitter.com/lemine_hamdinou",
    instagram: "https://www.instagram.com/lemin_med/",
    // Instagram, Twitter and Kaggle are also supported in the links!
    // To customize icons and social links, tweak src/components/SocialMedia
    display: true, // Set true to display this section, defaults to false
  };

  if (!socialMediaLinks.display) {
    return null;
  }

  return (
    <>
      <div className="h-54">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#06b6d4"
            fill-opacity="1"
            d="M0,32L48,80C96,128,192,224,288,272C384,320,480,320,576,288C672,256,768,192,864,176C960,160,1056,192,1152,213.3C1248,235,1344,245,1392,250.7L1440,256L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
          ></path>
        </svg>
      </div>



      <div className="bg-cyan-500 w-screen flex justify-center">
        <div className="md:w-full w-1/2">
          <div className="p-10 font-mont text-center">
            <p className="text-white pb-5">Designed and Developed by</p>
            <div className="h-1 border-2 border-white border-dotted"></div>
            <div className="flex w-full justify-between py-3 bg-white p-2">
              <IconContext.Provider
                value={{ className: "top-react-icons text-4xl" }}
              >
                {socialMediaLinks.github ? (
                  <a
                    href={socialMediaLinks.github}
                    className="icon-button github"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <FaGithub color="#333" className="" />
                  </a>
                ) : null}

                {socialMediaLinks.linkedin ? (
                  <a
                    href={socialMediaLinks.linkedin}
                    className="icon-button linkedin rounded-full"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <FaLinkedin color="#0e76a8" className="" />
                  </a>
                ) : null}

                {socialMediaLinks.gmail ? (
                  <a
                    href={`mailto:${socialMediaLinks.gmail}`}
                    className="icon-button google"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <GrMail color="#ea4335" className="" />
                  </a>
                ) : null}

                {socialMediaLinks.gitlab ? (
                  <a
                    href={socialMediaLinks.gitlab}
                    className="icon-button gitlab"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <FaGitlab color="#fca326" className="" />
                  </a>
                ) : null}

                {socialMediaLinks.facebook ? (
                  <a
                    href={socialMediaLinks.facebook}
                    className="icon-button facebook"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <FaFacebook color="#0f91f3" className="" />
                  </a>
                ) : null}

                {socialMediaLinks.instagram ? (
                  <a
                    href={socialMediaLinks.instagram}
                    className="icon-button instagram"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <FaInstagram color="#c13584" className="" />
                  </a>
                ) : null}

                {socialMediaLinks.twitter ? (
                  <a
                    href={socialMediaLinks.twitter}
                    className="icon-button twitter"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <FaTwitter color="#3498d8" className="" />
                  </a>
                ) : null}
              </IconContext.Provider>
            </div>
            <div className="h-1 border-2 border-gray-50 border-dotted"></div>
            <p className="text-white my-2 pt-5">Copyright &copy; 2022 Hamdinou Mohamed Lemine</p>
          </div>
        </div>
      </div>
    </>
  );
}
