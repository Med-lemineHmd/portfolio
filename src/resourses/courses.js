const coursesData = [
  {
    title: "React Developer Course",
    image:
      "https://ra9.dev/images/articles/react-fp-or-oop.jpg",

    link: "/",
  },
  {
    title: "Angular Developer Course",
    image:
      "https://hdwallpaperim.com/wp-content/uploads/2017/08/25/461803-angular-JavaScript-HTML.jpg",

    link: "/",
  },
  {
    title: "Node JS Course",
    image:
      "https://bs-uploads.toptal.io/blackfish-uploads/components/blog_post_page/content/cover_image_file/cover_image/686461/cover-secure-rest-api-in-nodejs-18f43b3033c239da5d2525cfd9fdc98f.png",

    link: "/",
  },
  {
    title: "MERN Stack Bootcamp",
    image:
      "https://res.cloudinary.com/practicaldev/image/fetch/s--2a-_OwgW--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/sm5uwpip9yglugcc15h4.png",

    link: "/",
  },
  {
    title: "MEAN Stack Bootcamp",
    image:
      "https://www.filepicker.io/api/file/BjBnqYkzR9KuQDnDuh7h",

    link: "/",
  },
  {
    title: "HTML+CSS+Bootstrap",
    image:
      "https://media.onlinecoursebay.com/2019/03/19033724/1890026_f604_4.jpg",

    link: "/",
  },
  {
    title: "Fullstack Web Development",
    image: "https://miro.medium.com/max/750/1*-hL6ZeXl_Kt0O-b82UQo9A.png",

    link: "/",
  },
  {
    title: "Javascript Mastery",
    image:
      "https://www.workato.com/product-hub/wp-content/uploads/2020/12/workato-blog-JavaScript-20201208-bl-01.jpg",

    link: "/",
  },
];

export default coursesData;
